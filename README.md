# 2023_assignment3_sport

# Requisiti per compilare:
- Java 17
- maven
# Requisiti per eseguire l'app:
- docker
- docker compose


# Env File
per la connessione all'istanza postgres, da parte dell' app dockerizzata, creare un file .env così:
```.env
SPORT_DB_USERNAME=postgres
SPORT_DB_PASSWORD=postgres
```

# Creazione jar e Immagine Docker
per far creare a maven il jar:
- `mvn package -Dskip.lint`
(sarà posizionato in target/)
per costruire l'immagine del servizio app, usando le variabili ambientali definite dal file .env:
- `sudo docker compose --env-file .env build app`

nota: se si modifica l'app, non basta ri-creare il jar, ma bisogna ricreare anche ricreare l'immagine dell'app.

# Caricare dump db (opzionale)
Dalla pagina dell'ultima release https://gitlab.com/frefolli/2023_assignment3_sport/-/releases si può caricare il file sql di un dump di dati. Lo si mette sotto la cartella ./restoring/ e, una volta eseguita l'app secondo le istruzioni sotto si può per caricare il dump nel db lanciare (da un altro terminale magari) il comando :
- `sudo docker compose exec db bash -c "psql -U postgres -h localhost -d sportdb < restoring/dump.sql"`
nota: in questo comando si è assunto che SPORT_DB_USERNAME=postgres e che il file dump si chiami dump.sql . 
Se si vuol modificare il nome della cartella da restoring a qualcos'altro basta farlo nel docker-compose.yml.


# Esecuzione dell'app
- `sudo docker compose run --service-ports app`
Si troverà alla pagina accedendo a `localhost:8080`` sulla macchina host.

# Cencellazione containers
per cancellare il container db creato con il comando precedente:
- `sudo docker compose down`

