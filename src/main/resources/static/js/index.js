class Manager {
    constructor(endpoint) {
        this.endpoint = endpoint;
    }

    index() {
        location.href = `${this.endpoint}`;
    }

    create() {
        location.href = `${this.endpoint}/new`;
    }

    show(id) {
        location.href = `${this.endpoint}/${id}`;
    }

    edit(id) {
        location.href = `${this.endpoint}/${id}/edit`;
    }

    destroy(id) {
        $.ajax({
		    type: 'POST',
			url: `${this.endpoint}/${id}/delete`,
            success: () => {
                this.index();
            },
            error: () => {
                PNotify.alert({text: "Impossibile eliminare l'elemento", type: 'error', delay: 2000});
            }
        })
    }
}

coachManager = new Manager("/coaches");
playerManager = new Manager("/players");
teamManager = new Manager("/teams");
matchManager = new Manager("/matches");
sponsorManager = new Manager("/sponsors");
stadiumManager = new Manager("/stadiums");
