package tbd.sport.controller;

import java.util.Optional;
import java.util.List;

import org.springframework.web.servlet.view.RedirectView;

import jakarta.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import tbd.sport.exception.TeamNotFoundException;
import tbd.sport.manager.CoachManager;
import tbd.sport.manager.TeamManager;
import tbd.sport.manager.StadiumManager;
import tbd.sport.model.Coach;
import tbd.sport.model.Team;
import tbd.sport.view.TeamViewer;

@Controller
public class TeamController {
	@Autowired
	private TeamManager teamManager;
	@Autowired
	private CoachManager coachManager;
	@Autowired
	private StadiumManager stadiumManager;

	@GetMapping("/teams")
	public ModelAndView index(@RequestParam Optional<String> name) {
		List<Team> teams = teamManager.find(name);
		return TeamViewer.Index(teams, name.orElse(""));
	}

	@GetMapping("/teams/{id}")
	public ModelAndView get(@PathVariable("id") Long id) {
		Optional<Team> team = teamManager.get(id);
		team.orElseThrow(TeamNotFoundException::new);
		return TeamViewer.Show(team.get());
	}

	@GetMapping("/teams/{id}/edit")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView modelAndView = new ModelAndView();

		Optional<Team> team = teamManager.get(id);
		team.orElseThrow(TeamNotFoundException::new);

		modelAndView.addObject("team", team.get());
		Coach coach = team.get().getCoach();
		Long coachId = null;
		if (coach != null) {
			coachId = coach.getId();
		}

		return TeamViewer.Edit(team.get(),
							   coachManager.getAllWithoutTeamOrSelected(coachId),
							   teamManager.getAllExcept(team.get().getId()),
							   stadiumManager.getAll());
	}

	@GetMapping("/teams/new")
	public ModelAndView new_() {
		return TeamViewer.New(Team.empty(),
							   coachManager.getAllWithoutTeamOrSelected(null),
							   teamManager.getAll(),
							   stadiumManager.getAll());
	}

	@PostMapping("/teams/create")
    public Object create(@Valid Team team, BindingResult bindingResult) {
      if (bindingResult.hasErrors()) {
    	  return TeamViewer.New(team,
							  coachManager.getAllWithoutTeamOrSelected(null),
							  teamManager.getAll(),
							  stadiumManager.getAll());
      }

      Team persistedTeam = (Team) teamManager.create(team);
      String url = String.format("/teams/%s", persistedTeam.getId());
      return new RedirectView(url);
    }

	@PostMapping("/teams/{id}/edit")
  public Object put(@Valid Team team, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      Coach coach = team.getCoach();
      Long coachId = null;
      if (coach != null) {
        coachId = coach.getId();
      }
      return TeamViewer.Edit(team,
						      coachManager.getAllWithoutTeamOrSelected(coachId),
						      teamManager.getAllExcept(team.getId()),
							  stadiumManager.getAll());
    }

    Team persistedTeam = teamManager.update(team);
    String url = String.format("/teams/%s", persistedTeam.getId());

    return new RedirectView(url);
  }

	@PostMapping("/teams/{id}/delete")
  public RedirectView delete(@PathVariable("id") Long id) {
      Optional<Team> team = teamManager.get(id);
      team.orElseThrow(TeamNotFoundException::new);
      teamManager.delete(team.get());
      return new RedirectView("/teams");
  }
}
