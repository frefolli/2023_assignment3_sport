package tbd.sport.controller;

import java.util.Optional;
import java.util.List;

import org.springframework.web.servlet.view.RedirectView;

import jakarta.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import tbd.sport.exception.SponsorNotFoundException;
import tbd.sport.manager.SponsorManager;
import tbd.sport.manager.TeamManager;
import tbd.sport.model.Sponsor;
import tbd.sport.view.SponsorViewer;

@Controller
public class SponsorController {
	@Autowired
	private SponsorManager sponsorManager;
	@Autowired
	private TeamManager teamManager;

	@GetMapping("/sponsors")
	public ModelAndView index(@RequestParam Optional<String> name) {
		List<Sponsor> sponsors = sponsorManager.find(name);
		return SponsorViewer.Index(sponsors, name.orElse(""));
	}

	@GetMapping("/sponsors/{id}")
	public ModelAndView get(@PathVariable("id") Long id) {
		Optional<Sponsor> sponsor = sponsorManager.get(id);
		sponsor.orElseThrow(SponsorNotFoundException::new);
		return SponsorViewer.Show(sponsor.get());
	}

	@GetMapping("/sponsors/{id}/edit")
	public ModelAndView edit(@PathVariable("id") Long id) {
		Optional<Sponsor> sponsor = sponsorManager.get(id);
		sponsor.orElseThrow(SponsorNotFoundException::new);
		return SponsorViewer.Edit(sponsor.get(), teamManager.getAll());
	}

	@GetMapping("/sponsors/new")
	public ModelAndView new_() {
		return SponsorViewer.New(Sponsor.empty(), teamManager.getAll());
	}

	@PostMapping("/sponsors/create")
  public Object create(@Valid Sponsor sponsor, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
		return SponsorViewer.New(sponsor, teamManager.getAll());
    }

    Sponsor persistedSponsor = (Sponsor) sponsorManager.create(sponsor);
    String url = String.format("/sponsors/%s", persistedSponsor.getId());
    return new RedirectView(url);
  }

	@PostMapping("/sponsors/{id}/edit")
  public Object put(@Valid Sponsor sponsor, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
		return SponsorViewer.Edit(sponsor, teamManager.getAll());
    }

    Sponsor persistedSponsor = sponsorManager.update(sponsor);
    String url = String.format("/sponsors/%s", persistedSponsor.getId());

    return new RedirectView(url);
  }

	@PostMapping("/sponsors/{id}/delete")
  public RedirectView delete(@PathVariable("id") Long id) {
    Optional<Sponsor> sponsor = sponsorManager.get(id);
    sponsor.orElseThrow(SponsorNotFoundException::new);
    sponsorManager.delete(sponsor.get());
    return new RedirectView("/sponsors");
  }
}
