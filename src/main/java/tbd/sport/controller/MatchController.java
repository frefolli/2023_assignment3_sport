package tbd.sport.controller;

import java.util.Optional;
import java.util.List;

import org.springframework.web.servlet.view.RedirectView;

import jakarta.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import tbd.sport.exception.MatchNotFoundException;
import tbd.sport.manager.TeamManager;
import tbd.sport.manager.MatchManager;
import tbd.sport.manager.StadiumManager;
import tbd.sport.model.Match;
import tbd.sport.view.MatchViewer;

@Controller
public class MatchController {
	@Autowired
	private MatchManager matchManager;
	@Autowired
	private TeamManager teamManager;
	@Autowired
	private StadiumManager stadiumManager;

	@GetMapping("/matches")
	public ModelAndView index() {
		List<Match> matches = matchManager.find();
        return MatchViewer.Index(matches);
	}

	@GetMapping("/matches/{id}")
	public ModelAndView get(@PathVariable("id") Long id) {
		Optional<Match> match = matchManager.get(id);
		match.orElseThrow(MatchNotFoundException::new);
        return MatchViewer.Show(match.get());
	}

	@GetMapping("/matches/{id}/edit")
	public ModelAndView edit(@PathVariable("id") Long id) {
		Optional<Match> match = matchManager.get(id);
		match.orElseThrow(MatchNotFoundException::new);
        return MatchViewer.Edit(match.get(), teamManager.getAll(), stadiumManager.getAll());
	}

	@GetMapping("/matches/new")
	public ModelAndView new_() {
	      return MatchViewer.New(Match.empty(), teamManager.getAll(), stadiumManager.getAll());
	}

	@PostMapping("/matches/create")
  public Object create(@Valid Match match, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
        return MatchViewer.New(match, teamManager.getAll(), stadiumManager.getAll());
    }

    Match persistedMatch = (Match) matchManager.create(match);
    String url = String.format("/matches/%s", persistedMatch.getId());
    return new RedirectView(url);
  }

	@PostMapping("/matches/{id}/edit")
  public Object put(@Valid Match match, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      return MatchViewer.Edit(match, teamManager.getAll(), stadiumManager.getAll());
    }

    Match persistedMatch = matchManager.update(match);
    String url = String.format("/matches/%s", persistedMatch.getId());

    return new RedirectView(url);
  }

	@PostMapping("/matches/{id}/delete")
  public RedirectView delete(@PathVariable("id") Long id) {
    Optional<Match> match = matchManager.get(id);
    match.orElseThrow(MatchNotFoundException::new);
    matchManager.delete(match.get());
    return new RedirectView("/matches");
  }
}
