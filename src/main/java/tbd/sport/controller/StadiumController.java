package tbd.sport.controller;

import java.util.Optional;
import java.util.List;

import org.springframework.web.servlet.view.RedirectView;

import jakarta.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import tbd.sport.exception.StadiumNotFoundException;
import tbd.sport.manager.StadiumManager;
import tbd.sport.model.Stadium;
import tbd.sport.view.StadiumViewer;

@Controller
public class StadiumController {
	@Autowired
	private StadiumManager stadiumManager;

	@GetMapping("/stadiums")
	public ModelAndView index(@RequestParam Optional<String> name) {
		List<Stadium> stadi = stadiumManager.find(name);
		return StadiumViewer.Index(stadi, name.orElse(""));
	}

	@GetMapping("/stadiums/{id}")
	public ModelAndView get(@PathVariable("id") Long id) {
		Optional<Stadium> stadium = stadiumManager.get(id);
		stadium.orElseThrow(StadiumNotFoundException::new);
		return StadiumViewer.Show(stadium.get());
	}

	@GetMapping("/stadiums/{id}/edit")
	public ModelAndView edit(@PathVariable("id") Long id) {
		Optional<Stadium> stadium = stadiumManager.get(id);
		stadium.orElseThrow(StadiumNotFoundException::new);
		return StadiumViewer.Edit(stadium.get());
	}

	@GetMapping("/stadiums/new")
	public ModelAndView new_() {
		return StadiumViewer.New(Stadium.empty());
	}

	@PostMapping("/stadiums/create")
  public Object create(@Valid Stadium stadium, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
		return StadiumViewer.New(stadium);
    }

    Stadium persistedStadium = stadiumManager.create(stadium);
    String url = String.format("/stadiums/%s", persistedStadium.getId());
    return new RedirectView(url);
  }

	@PostMapping("/stadiums/{id}/edit")
  public Object put(@Valid Stadium stadium, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
		return StadiumViewer.Edit(stadium);
    }

    Stadium persistedStadium = stadiumManager.update(stadium);
    String url = String.format("/stadiums/%s", persistedStadium.getId());
    return new RedirectView(url);
  }

	@PostMapping("/stadiums/{id}/delete")
  public RedirectView delete(@PathVariable("id") Long id) {
    Optional<Stadium> stadium = stadiumManager.get(id);
    stadium.orElseThrow(StadiumNotFoundException::new);
    stadiumManager.delete(stadium.get());
    return new RedirectView("/stadiums");
  }
}
