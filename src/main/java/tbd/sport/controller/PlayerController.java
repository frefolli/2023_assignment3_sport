package tbd.sport.controller;

import java.util.Optional;
import java.util.List;

import org.springframework.web.servlet.view.RedirectView;

import jakarta.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;

import tbd.sport.exception.PlayerNotFoundException;
import tbd.sport.manager.PlayerManager;
import tbd.sport.manager.TeamManager;
import tbd.sport.model.Color;
import tbd.sport.model.Player;
import tbd.sport.view.PlayerViewer;

@Controller
public class PlayerController {
	@Autowired
	private PlayerManager playerManager;
	@Autowired
	private TeamManager teamManager;

	@GetMapping("/players")
	public ModelAndView index(@RequestParam Optional<String> firstName,
							  @RequestParam Optional<String> lastName,
							  @RequestParam Optional<String> city,
							  @RequestParam Optional<Color> color) {
		List<Player> players = playerManager.find(firstName.orElse(""),
												  lastName.orElse(""),
												  city.orElse(""),
												  color.orElse(null));
		return PlayerViewer.Index(players,
								  firstName.orElse(""),
								  lastName.orElse(""),
								  city.orElse(""),
								  color.orElse(null));
	}

	@GetMapping("/players/{id}")
	public ModelAndView get(@PathVariable("id") Long id) {
		Optional<Player> player = playerManager.get(id);
		player.orElseThrow(PlayerNotFoundException::new);

		return PlayerViewer.Edit(player.get(), teamManager.getAll());
	}

	@GetMapping("/players/{id}/edit")
	public ModelAndView edit(@PathVariable("id") Long id) {
		Optional<Player> player = playerManager.get(id);
		player.orElseThrow(PlayerNotFoundException::new);

		return PlayerViewer.Edit(player.get(), teamManager.getAll());
	}

	@GetMapping("/players/new")
	public ModelAndView new_() {
		return PlayerViewer.New(Player.empty(), teamManager.getAll());
	}

	@PostMapping("/players/create")
	public Object create(@Valid Player player, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return PlayerViewer.New(player, teamManager.getAll());
		}

		Player persistedPlayer = playerManager.create(player);
		String url = String.format("/players/%s", persistedPlayer.getId());
		return new RedirectView(url);
	}

	@PostMapping("/players/{id}/edit")
	public Object put(@Valid Player player, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			return PlayerViewer.Edit(player, teamManager.getAll());
		}

		Player persistedPlayer = playerManager.update(player);
		String url = String.format("/players/%s", persistedPlayer.getId());
		return new RedirectView(url);
	}

	@PostMapping("/players/{id}/delete")
	public RedirectView delete(@PathVariable("id") Long id) {
		Optional<Player> player = playerManager.get(id);
		player.orElseThrow(PlayerNotFoundException::new);
		playerManager.delete(player.get());
		return new RedirectView("/players");
	}
}
