package tbd.sport.controller;

import java.util.Optional;
import java.util.List;

import org.springframework.web.servlet.view.RedirectView;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import tbd.sport.exception.CoachNotFoundException;
import tbd.sport.manager.CoachManager;
import tbd.sport.model.Coach;
import tbd.sport.model.Color;
import tbd.sport.model.Team;
import tbd.sport.exception.TeamNotFoundException;
import tbd.sport.manager.TeamManager;
import tbd.sport.view.CoachViewer;

@Controller
public class CoachController {
	@Autowired
	private CoachManager coachManager;
	@Autowired
	private TeamManager teamManager;

	@GetMapping("/coaches")
	public ModelAndView index(@RequestParam Optional<String> firstName,
							  @RequestParam Optional<String> lastName,
							  @RequestParam Optional<String> city,
							  @RequestParam Optional<Color> color) {
		List<Coach> coaches = coachManager.find(firstName.orElse(""),
												lastName.orElse(""),
												city.orElse(""),
												color.orElse(null));
		return CoachViewer.Index(coaches,
								  firstName.orElse(""),
								  lastName.orElse(""),
								  city.orElse(""),
								  color.orElse(null));
	}

	@GetMapping("/coaches/{id}")
	public ModelAndView get(@PathVariable("id") Long id) {
		Optional<Coach> coach = coachManager.get(id);
		coach.orElseThrow(CoachNotFoundException::new);
		return CoachViewer.Show(coach.get());
	}

	@GetMapping("/coaches/{id}/edit")
	public ModelAndView edit(@PathVariable("id") Long id) {
		Optional<Coach> coach = coachManager.get(id);
		coach.orElseThrow(CoachNotFoundException::new);
		return CoachViewer.Edit(coach.get(), teamManager.getAll());
	}

	@GetMapping("/coaches/new")
	public ModelAndView new_() {
		return CoachViewer.New(Coach.empty(), teamManager.getAll());
	}

	@PostMapping("/coaches/create")	
  	public Object create(@Valid Coach coach, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
		    return CoachViewer.New(coach, teamManager.getAll());
		}
		
		Coach persistedCoach = coachManager.create(coach);

		if (null != coach.getTeam()) {
			Long teamId = coach.getTeam().getId();
			Optional<Team> team = teamManager.get(teamId);
			team.orElseThrow(TeamNotFoundException::new);
			team.get().setCoach(coach);
	    	teamManager.update(team.get());	
		}

		String url = String.format("/coaches/%s", persistedCoach.getId());
		return new RedirectView(url);
  }


	@PostMapping("/coaches/{id}/edit")
  	public Object put(@Valid Coach coach, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
		    return CoachViewer.Edit(coach, teamManager.getAll());
		}

		Optional<Team> previousCoachedTeam = teamManager.findTeamCoachedBy(coach);
		if (previousCoachedTeam.isPresent()){  
			previousCoachedTeam.get().setCoach(null);
    		teamManager.update(previousCoachedTeam.get());
		}

        teamManager.assignCoach(coach);

		Coach persistedCoach = coachManager.update(coach);
		String url = String.format("/coaches/%s", persistedCoach.getId());
		return new RedirectView(url);
	}	

	@PostMapping("/coaches/{id}/delete")
	public RedirectView delete(@PathVariable("id") Long id) {
		Optional<Coach> coach = coachManager.get(id);
		coach.orElseThrow(CoachNotFoundException::new);
		coachManager.delete(coach.get());
		return new RedirectView("/coaches");
	}
}
