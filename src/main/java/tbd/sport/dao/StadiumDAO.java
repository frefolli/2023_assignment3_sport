package tbd.sport.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tbd.sport.model.Stadium;

@Repository
public interface StadiumDAO extends JpaRepository<Stadium, Long> {
	Optional<Stadium> findById(Long id);
	Stadium save(Stadium stadium);
	List<Stadium> findByNameContains(String name);
	void deleteById(Long id);
}
