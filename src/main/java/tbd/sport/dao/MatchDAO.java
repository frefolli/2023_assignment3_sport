package tbd.sport.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tbd.sport.model.Match;

@Repository
public interface MatchDAO extends JpaRepository<Match, Long> {
	Optional<Match> findById(Long id);
	Match save(Match team);
	void deleteById(Long id);
}