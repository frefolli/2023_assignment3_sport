package tbd.sport.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import tbd.sport.model.Color;
import tbd.sport.model.Player;

@Repository
public interface PlayerDAO extends PersonDAO<Player> {
	Optional<Player> findById(Long id);
	Player save(Player player);
	void deleteById(Long id);
	
	List<Player> findByFirstNameContainsAndLastNameContainsAndTeamCityContains(String firstName, String lastName, String city);
	List<Player> findByFirstNameContainsAndLastNameContainsAndTeamCityContainsAndTeamColor(String firstName, String lastName, String city, Color color);
	List<Player> findByFirstNameContainsAndLastNameContainsAndTeamColor(String firstName, String lastName, Color color);
}
