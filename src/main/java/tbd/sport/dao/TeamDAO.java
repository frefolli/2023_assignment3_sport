package tbd.sport.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import tbd.sport.model.Team;

@Repository
public interface TeamDAO extends JpaRepository<Team, Long> {
	Optional<Team> findById(Long id);
	Team save(Team team);
	void deleteById(Long id);
	List<Team> findByNameContains(String name);
	
	@Query(value = "select * from team where id <> :teamId and (parent_id <> :teamId OR parent_id IS NULL)", nativeQuery = true)
	List<Team> getAllExcept(@Param("teamId") Long teamId);


	@Query(value = "select * from team where coach_id = :coach_id", nativeQuery = true)
	List<Team> findTeamCoachedBy(@Param("coach_id") Long coach_id);

}

