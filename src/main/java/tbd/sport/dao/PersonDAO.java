package tbd.sport.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import tbd.sport.model.Person;

@Repository
public interface PersonDAO<P extends Person> extends JpaRepository<P, Long> {
	Optional<P> findById(Long id);
	void deleteById(Long id);
	List<P> findByFirstNameContains(String firstName);
	List<P> findByLastNameContains(String lastName);
	List<P> findByFirstNameContainsAndLastNameContains(String firstName, String lastName);
}
