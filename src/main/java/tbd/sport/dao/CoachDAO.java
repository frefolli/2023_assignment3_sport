package tbd.sport.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import tbd.sport.model.Coach;
import tbd.sport.model.Color;

@Repository
public interface CoachDAO extends PersonDAO<Coach> {
	Optional<Coach> findById(Long id);
	Coach save(Coach coach);
	void deleteById(Long id);
	
	@Query(value = "select * from person where dtype = 'Coach' and (id NOT IN (select team.coach_id from team where team.coach_id is not null) or id = :coachId)", nativeQuery = true)
	List<Coach> getAllWithoutATeamOrSelected(@Param("coachId") Long coachId);
	List<Coach> findByFirstNameContainsAndLastNameContainsAndTeamCityContains(String firstName, String lastName,
			String city);
	List<Coach> findByFirstNameContainsAndLastNameContainsAndTeamCityContainsAndTeamColor(String firstName,
			String lastName, String city, Color color);
	List<Coach> findByFirstNameContainsAndLastNameContainsAndTeamColor(String firstName, String lastName, Color color);
}
