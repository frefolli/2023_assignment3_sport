package tbd.sport.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import tbd.sport.model.Sponsor;

@Repository
public interface SponsorDAO extends JpaRepository<Sponsor, Long> {
	Optional<Sponsor> findById(Long id);
	Sponsor save(Sponsor sponsor);
	void deleteById(Long id);
	List<Sponsor> findByNameContains(String name);
}

