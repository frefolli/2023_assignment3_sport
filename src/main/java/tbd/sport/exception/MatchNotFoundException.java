package tbd.sport.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="No such Match")
public class MatchNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -8252953614783756469L;

  public MatchNotFoundException() {
    super();
  }

  public MatchNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public MatchNotFoundException(String message) {
    super(message);
  }

  public MatchNotFoundException(Throwable cause) {
    super(cause);
  }
}
