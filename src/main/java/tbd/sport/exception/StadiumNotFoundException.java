package tbd.sport.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="No such Stadium")
public class StadiumNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -8252953614783756469L;

  public StadiumNotFoundException() {
    super();
  }

  public StadiumNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public StadiumNotFoundException(String message) {
    super(message);
  }

  public StadiumNotFoundException(Throwable cause) {
    super(cause);
  }
}
