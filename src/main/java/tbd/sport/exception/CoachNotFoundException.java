package tbd.sport.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="No such Coach")
public class CoachNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -8252953614783756469L;

  public CoachNotFoundException() {
    super();
  }

  public CoachNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public CoachNotFoundException(String message) {
    super(message);
  }

  public CoachNotFoundException(Throwable cause) {
    super(cause);
  }
}
