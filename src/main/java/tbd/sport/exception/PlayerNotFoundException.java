package tbd.sport.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="No such Player")
public class PlayerNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -8252953614783756469L;

  public PlayerNotFoundException() {
    super();
  }

  public PlayerNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public PlayerNotFoundException(String message) {
    super(message);
  }

  public PlayerNotFoundException(Throwable cause) {
    super(cause);
  }
}
