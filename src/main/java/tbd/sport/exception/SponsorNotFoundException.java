package tbd.sport.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="No such Sponsor")
public class SponsorNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -8252953614783756469L;

  public SponsorNotFoundException() {
    super();
  }

  public SponsorNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public SponsorNotFoundException(String message) {
    super(message);
  }

  public SponsorNotFoundException(Throwable cause) {
    super(cause);
  }
}
