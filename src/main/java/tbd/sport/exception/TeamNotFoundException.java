package tbd.sport.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason="No such Team")
public class TeamNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -8252953614783756469L;

  public TeamNotFoundException() {
    super();
  }

  public TeamNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }

  public TeamNotFoundException(String message) {
    super(message);
  }

  public TeamNotFoundException(Throwable cause) {
    super(cause);
  }
}
