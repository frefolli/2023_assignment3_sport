package tbd.sport.view;

import org.springframework.web.servlet.ModelAndView;

public class ViewBuilder {
	ModelAndView modelAndView;
	
	protected ViewBuilder(String path) {
		modelAndView = new ModelAndView();
		modelAndView.setViewName(path);
	}
	
	public static ViewBuilder of(String path) {
		return new ViewBuilder(path);
	}
	
	public ViewBuilder with(String name, Object object) {
		modelAndView.addObject(name, object);
		return this;
	}

	public ModelAndView ok() {
		return modelAndView;
	}
}
