package tbd.sport.view;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import tbd.sport.model.Coach;
import tbd.sport.model.Color;
import tbd.sport.model.Stadium;
import tbd.sport.model.Team;

public class TeamViewer {
	public static ModelAndView Index(List<Team> teams, String name) {
		return ViewBuilder.of("teams/index")
						  .with("teams", teams)
						  .with("name", name)
						  .with("colors", Color.values()).ok();
	}

	public static ModelAndView New(Team team, List<Coach> coaches,  List<Team> parents, List<Stadium> stadiums) {
		return ViewBuilder.of("teams/new")
				  .with("team", team)
				  .with("coaches", coaches)
				  .with("teams", parents)
				  .with("stadiums", stadiums)
				  .with("colors", Color.values()).ok();
	}

	public static ModelAndView Show(Team team) {
		return ViewBuilder.of("teams/show")
				  .with("team", team).ok();
	}

	public static ModelAndView Edit(Team team, List<Coach> coaches,  List<Team> parents, List<Stadium> stadiums) {
		return ViewBuilder.of("teams/edit")
				  .with("team", team)
				  .with("coaches", coaches)
				  .with("teams", parents)
				  .with("stadiums", stadiums)
				  .with("colors", Color.values()).ok();
	}
}