package tbd.sport.view;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import tbd.sport.model.Match;
import tbd.sport.model.Stadium;
import tbd.sport.model.Team;

public class MatchViewer {
	public static ModelAndView Index(List<Match> matches) {
		return ViewBuilder.of("matches/index")
						  .with("matches", matches).ok();
	}

	public static ModelAndView New(Match match, List<Team> teams, List<Stadium> stadiums) {
		return ViewBuilder.of("matches/new")
				  .with("match", match)
				  .with("teams", teams)
				  .with("stadiums", stadiums).ok();
	}

	public static ModelAndView Show(Match match) {
		return ViewBuilder.of("matches/show")
				  .with("match", match).ok();
	}

	public static ModelAndView Edit(Match match, List<Team> teams, List<Stadium> stadiums) {
		return ViewBuilder.of("matches/edit")
				  .with("match", match)
				  .with("teams", teams)
				  .with("stadiums", stadiums).ok();
	}
}