package tbd.sport.view;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import tbd.sport.model.Stadium;

public class StadiumViewer {
	public static ModelAndView Index(List<Stadium> stadiums, String name) {
		return ViewBuilder.of("stadiums/index")
						  .with("stadiums", stadiums)
						  .with("name", name).ok();
	}

	public static ModelAndView New(Stadium stadium) {
		return ViewBuilder.of("stadiums/new")
				  .with("stadium", stadium).ok();
	}

	public static ModelAndView Show(Stadium stadium) {
		return ViewBuilder.of("stadiums/show")
				  .with("stadium", stadium).ok();
	}

	public static ModelAndView Edit(Stadium stadium) {
		return ViewBuilder.of("stadiums/edit")
				  .with("stadium", stadium).ok();
	}
}
