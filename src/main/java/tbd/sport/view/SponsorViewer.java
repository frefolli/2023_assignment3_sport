package tbd.sport.view;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import tbd.sport.model.Sponsor;
import tbd.sport.model.Team;

public class SponsorViewer {
	public static ModelAndView Index(List<Sponsor> sponsors, String name) {
		return ViewBuilder.of("sponsors/index")
						  .with("sponsors", sponsors)
						  .with("name", name).ok();
	}

	public static ModelAndView New(Sponsor sponsor, List<Team> teams) {
		return ViewBuilder.of("sponsors/new")
				  .with("sponsor", sponsor)
				  .with("teams", teams).ok();
	}

	public static ModelAndView Show(Sponsor sponsor) {
		return ViewBuilder.of("sponsors/show")
				  .with("sponsor", sponsor).ok();
	}

	public static ModelAndView Edit(Sponsor sponsor, List<Team> teams) {
		return ViewBuilder.of("sponsors/edit")
				  .with("sponsor", sponsor)
				  .with("teams", teams).ok();
	}
}
