package tbd.sport.view;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import tbd.sport.model.Color;
import tbd.sport.model.Player;
import tbd.sport.model.Team;

public class PlayerViewer {
	public static ModelAndView Index(List<Player> players,
									 String firstName,
									 String lastName,
									 String city,
									 Color color) {
		return ViewBuilder.of("players/index")
						  .with("players", players)
						  .with("firstName", firstName)
						  .with("lastName", lastName)
						  .with("city", city)
						  .with("color", color)
						  .with("colors", Color.values()).ok();
	}

	public static ModelAndView New(Player player, List<Team> teams) {
		return ViewBuilder.of("players/new")
				  .with("player", player)
				  .with("teams", teams).ok();
	}

	public static ModelAndView Show(Player player) {
		return ViewBuilder.of("players/show")
				  .with("player", player).ok();
	}

	public static ModelAndView Edit(Player player, List<Team> teams) {
		return ViewBuilder.of("players/edit")
				  .with("player", player)
				  .with("teams", teams).ok();
	}
}
