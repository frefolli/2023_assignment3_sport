package tbd.sport.view;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import tbd.sport.model.Coach;
import tbd.sport.model.Color;
import tbd.sport.model.Team;

public class CoachViewer {
	public static ModelAndView Index(List<Coach> coaches, String firstName, String lastName, String city, Color color) {
		return ViewBuilder.of("coaches/index")
						  .with("coaches", coaches)
						  .with("firstName", firstName)
						  .with("lastName", lastName)
						  .with("city", city)
						  .with("color", color)
						  .with("colors", Color.values()).ok();
	}

	public static ModelAndView New(Coach coach, List<Team> teams) {
		return ViewBuilder.of("coaches/new")
				  .with("coach", coach)
				  .with("teams", teams).ok();
	}

	public static ModelAndView Show(Coach coach) {
		return ViewBuilder.of("coaches/show")
				  .with("coach", coach).ok();
	}

	public static ModelAndView Edit(Coach coach, List<Team> teams) {
		return ViewBuilder.of("coaches/edit")
				  .with("coach", coach)
				  .with("teams", teams).ok();
	}
}
