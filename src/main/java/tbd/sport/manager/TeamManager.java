package tbd.sport.manager;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tbd.sport.model.Team;
import tbd.sport.model.Coach;
import tbd.sport.dao.TeamDAO;
import tbd.sport.exception.TeamNotFoundException;
import java.util.Optional;

@Component
public class TeamManager {
	@Autowired
	private TeamDAO teamDAO;

	public Team create(Team team) {
		return teamDAO.save(team);
	}

	public List<Team> getAll() {
		return teamDAO.findAll();
	}

	public List<Team> find(Optional<String> name) {
		if (name.isPresent() && !name.get().isBlank()) {
			return teamDAO.findByNameContains(name.get());
		} else {
			return teamDAO.findAll();
		}
	}

	public Optional<Team> findTeamCoachedBy(Coach c) {
		List<Team> result = teamDAO.findTeamCoachedBy(c.getId());
		if (result.size() > 0) {
			return Optional.of(result.get(0));
		}
		return Optional.empty();
	}

	public Optional<Team> get(Long id) {
		return teamDAO.findById(id);
	}

	public Team update(Team team) {
		return teamDAO.save(team);
	}

	public void delete(Team team) {
		teamDAO.delete(team);
	}

	public List<Team> getAllExcept(Long id) {
		return teamDAO.getAllExcept(id);
	}

	public void assignCoach(Coach coach) {
		if (null != coach.getTeam()) {
			Long teamId = coach.getTeam().getId();
			Optional<Team> team = get(teamId);
			team.orElseThrow(TeamNotFoundException::new);
			team.get().setCoach(coach);
			update(team.get());
		}
	}
}

