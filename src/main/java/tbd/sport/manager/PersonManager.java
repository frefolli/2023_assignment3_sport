package tbd.sport.manager;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import tbd.sport.dao.PersonDAO;
import tbd.sport.model.Person;

@Component
public class PersonManager<P extends Person> {
	protected PersonDAO<P> personDAO;

	public PersonManager(PersonDAO<P> personDAO) {
		this.personDAO = personDAO;
	}

	// Create
	public P create(P person) {
		return personDAO.save(person);
	}

	// Read
	public List<P> getAll() {
		return personDAO.findAll();
	}

	public Optional<P> get(Long id) {
		return personDAO.findById(id);
	}

	// Update
	public P update(P person) {
		return personDAO.save(person);
	}

	// Delete
	public void delete(P person) {
		personDAO.delete(person);
	}
}
