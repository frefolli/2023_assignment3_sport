package tbd.sport.manager;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tbd.sport.dao.SponsorDAO;
import tbd.sport.model.Sponsor;
import java.util.Optional;

@Component
public class SponsorManager {
	@Autowired
	private SponsorDAO sponsorDAO;

	public Sponsor create(Sponsor sponsor) {
		return sponsorDAO.save(sponsor);
	}

	public List<Sponsor> getAll() {
		return sponsorDAO.findAll();
	}

	public List<Sponsor> find(Optional<String> name) {
		if (name.isPresent() && !name.get().isBlank()) {
			return sponsorDAO.findByNameContains(name.get());
		} else {
			return sponsorDAO.findAll();
		}
	}

	public Optional<Sponsor> get(Long id) {
		return sponsorDAO.findById(id);
	}

	public Sponsor update(Sponsor sponsor) {
		return sponsorDAO.save(sponsor);
	}

	public void delete(Sponsor sponsor) {
		sponsorDAO.delete(sponsor);
	}
}

