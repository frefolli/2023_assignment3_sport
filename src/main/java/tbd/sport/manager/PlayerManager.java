package tbd.sport.manager;

import java.util.List;

import org.springframework.stereotype.Component;

import tbd.sport.dao.PlayerDAO;
import tbd.sport.model.Color;
import tbd.sport.model.Player;

@Component
public class PlayerManager extends PersonManager<Player> {
	PlayerDAO playerDAO;
	public PlayerManager(PlayerDAO playerDAO) {
		super(playerDAO);
		this.playerDAO = playerDAO;
	}

	public List<Player> find(String firstName, String lastName, String city, Color color) {
		if (firstName.isBlank())
			firstName = "";
		if (lastName.isBlank())
			lastName = "";

		if (city.isBlank()) {
			if (null == color) {
				return playerDAO.findByFirstNameContainsAndLastNameContains(firstName, lastName);
			}
			return playerDAO.findByFirstNameContainsAndLastNameContainsAndTeamColor(firstName, lastName, color);
		} else {
			if (null == color) {
				return playerDAO.findByFirstNameContainsAndLastNameContainsAndTeamCityContains(firstName, lastName, city);
			}
			return playerDAO.findByFirstNameContainsAndLastNameContainsAndTeamCityContainsAndTeamColor(firstName, lastName, city, color);
		}
	}
}
