package tbd.sport.manager;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tbd.sport.dao.StadiumDAO;
import tbd.sport.model.Stadium;

@Component
public class StadiumManager {
	@Autowired
	private StadiumDAO stadiumDAO;

	public Stadium create(Stadium stadium) {
		return stadiumDAO.save(stadium);
	}

	public List<Stadium> getAll() {
		return stadiumDAO.findAll();
	}

	public List<Stadium> find(Optional<String> name) {
		if (name.isPresent() && !name.get().isBlank()) {
			return stadiumDAO.findByNameContains(name.get());
		} else {
			return stadiumDAO.findAll();
		}
	}

	public Optional<Stadium> get(Long id) {
		return stadiumDAO.findById(id);
	}

	public Stadium update(Stadium stadio) {
		return stadiumDAO.save(stadio);
	}

	public void delete(Stadium stadio) {
		stadiumDAO.delete(stadio);
	}
}
