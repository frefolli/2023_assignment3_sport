package tbd.sport.manager;

import java.util.List;

import org.springframework.stereotype.Component;

import tbd.sport.dao.CoachDAO;
import tbd.sport.model.Coach;
import tbd.sport.model.Color;

@Component
public class CoachManager extends PersonManager<Coach> {
	private CoachDAO coachDAO;

	public CoachManager(CoachDAO coachDAO) {
		super(coachDAO);
		this.coachDAO = coachDAO;
	}

	public List<Coach> getAllWithoutTeamOrSelected(Long selected) {
		return coachDAO.getAllWithoutATeamOrSelected(selected);
	}

	public List<Coach> find(String firstName, String lastName, String city, Color color) {
		if (firstName.isBlank())
			firstName = "";
		if (lastName.isBlank())
			lastName = "";
		if (city.isBlank()) {
			if (null == color) {
				return coachDAO.findByFirstNameContainsAndLastNameContains(firstName, lastName);
			}
			return coachDAO.findByFirstNameContainsAndLastNameContainsAndTeamColor(firstName, lastName, color);
		} else {
			if (null == color) {
				return coachDAO.findByFirstNameContainsAndLastNameContainsAndTeamCityContains(firstName, lastName, city);
			}
			return coachDAO.findByFirstNameContainsAndLastNameContainsAndTeamCityContainsAndTeamColor(firstName, lastName, city, color);
		}
	}
}
