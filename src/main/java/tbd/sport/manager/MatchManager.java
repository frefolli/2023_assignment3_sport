package tbd.sport.manager;
import java.util.List;
import org.springframework.stereotype.Component;

import tbd.sport.dao.MatchDAO;
import tbd.sport.model.Match;

import java.util.Optional;

@Component
public class MatchManager {
	private MatchDAO teamDAO;

	public MatchManager(MatchDAO teamDAO) {
		this.teamDAO = teamDAO;
	}

	public Match create(Match team) {
		return teamDAO.save(team);
	}

	public List<Match> getAll() {
		return teamDAO.findAll();
	}

	public List<Match> find() {
		return teamDAO.findAll();
	}

	public Optional<Match> get(Long id) {
		return teamDAO.findById(id);
	}

	public Match update(Match team) {
		return teamDAO.save(team);
	}

	public void delete(Match team) {
		teamDAO.delete(team);
	}
}

