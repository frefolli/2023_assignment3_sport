package tbd.sport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.context.annotation.ComponentScan;

@EnableJpaRepositories("tbd.sport.dao")
@ComponentScan("tbd.sport.manager")
@EntityScan("tbd.sport.model")
@SpringBootApplication
public class Application {
 
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

}
