package tbd.sport.model;

import java.util.Set;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinTable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.validation.constraints.NotBlank;

@Entity
public class Sponsor {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@NotBlank
	private String name;
	@ManyToMany
	@JoinTable(name = "sponsorship", 
	joinColumns = @JoinColumn(name = "sponsor_id"), 
	inverseJoinColumns = @JoinColumn(name = "team_id"))
	private Set<Team> teams;

	protected Sponsor() {
	}

	public Sponsor(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public Set<Team> getTeams() {
		return teams;
	}

	public void setTeams(Set<Team> teams) {
		this.teams = teams;
	}

	static public Sponsor empty(){
		return new Sponsor();
	}
}
