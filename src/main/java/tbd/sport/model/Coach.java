package tbd.sport.model;

import java.util.Objects;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;

@Entity
public class Coach extends Person {
	@OneToOne(mappedBy = "coach")
	private Team team;

	protected Coach() {}

	@Override
	public String toString() {
		return String.format("%s %s", getFirstName(), getLastName());
	}

	public static Coach empty() {
		return new Coach();
	}
	
	public Team getTeam() {
		return team;
	}
	
	public void setTeam(Team team) {
		this.team = team;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId());
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof Coach) {
			super.equals(other);
		}
		return false;
	}
}
