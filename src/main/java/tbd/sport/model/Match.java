package tbd.sport.model;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.AssertTrue;
import jakarta.validation.constraints.Min;

@Entity
public class Match {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Min(value=0)
  private Long homeGoals;
  @Min(value=0)
  private Long awayGoals;
  
  @DateTimeFormat(pattern = "yyyy-MM-dd")
  private Date at;
  
  @NotNull
  @ManyToOne
  @JoinColumn(name = "home_id")
  private Team home;
  
  @NotNull
  @ManyToOne
  @JoinColumn(name = "away_id")
  private Team away;
  
  @NotNull
  @ManyToOne
  @JoinColumn(name = "stadium_id")
  private Stadium stadium;

  protected Match() {
  }

  @AssertTrue(message = "A team cannot match against itself")
  public boolean isHomeAndAwayNotEqual() {
    return null == home || null == away || !home.equals(away);
  }

  @AssertTrue(message = "Should specify full score or none")
  public boolean isFullScoreOrNone() {
    if (null != homeGoals && null != awayGoals)
        return true;
    if (null == homeGoals && null == awayGoals)
        return true;
    return false;
  }

  @Override
  public String toString() {
    return home + " - " + away;
  }

  public String getScore() {
    if (null != homeGoals)
      return homeGoals + " - " + awayGoals;
    return "";
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  static public Match empty(){
    return new Match();
  }
	
	public Team getHome() {
		return home;
	}
	
	public void setHome(Team home) {
		this.home = home;
	}
	
	public Team getAway() {
		return away;
	}
	
	public void setAway(Team away) {
		this.away = away;
	}
	
	public Stadium getStadium() {
		return stadium;
	}
	
	public void setStadium(Stadium stadium) {
		this.stadium = stadium;
	}

    public void setAt(Date at) {
        this.at = at;
    }

    public void setHomeGoals(Long goals) {
        this.homeGoals = goals;
    }

    public void setAwayGoals(Long goals) {
        this.awayGoals = goals;
    }

    public Date getAt() {
        return this.at;
    }

    public Long getHomeGoals() {
        return this.homeGoals;
    }

    public Long getAwayGoals() {
        return this.awayGoals;
    }
}
