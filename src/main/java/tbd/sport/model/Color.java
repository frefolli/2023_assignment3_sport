package tbd.sport.model;

public enum Color {
	BLACK, BLUE, RED, YELLOW, GREEN, ORANGE, PURPLE, WHITE
}
