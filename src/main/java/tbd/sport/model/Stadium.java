package tbd.sport.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.NotBlank;

@Entity
public class Stadium {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  @NotBlank
  private String name;
  @NotNull
  private Integer capacity;

  protected Stadium() {}

  public Stadium(String name, Integer capacity) {
    setName(name);
    setCapacity(capacity);
  }

  @Override
  public String toString() {
    return name;
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }
  
  public Integer getCapacity() {
	  return capacity;
  }
  
  public void setId(Long id) {
	  this.id = id;
  }
  
  public void setName(String name) {
	  this.name = name;
  }
  
  public void setCapacity(Integer capacity) {
	  this.capacity = capacity;
  }
  
  public static Stadium empty() {
	    return new Stadium();
  }
}
