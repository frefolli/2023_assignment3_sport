package tbd.sport.model;

import java.util.Objects;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class Player extends Person {
	@ManyToOne
    @JoinColumn(name = "team_id")
	private Team team;

	protected Player() {}

	@Override
	public String toString() {
		return String.format("Player[id=%d, firstName='%s', lastName='%s']", getId(), getFirstName(), getLastName());
	}

	public static Player empty() {
		return new Player();
	}
	
	public Team getTeam() {
		return team;
	}
	
	public void setTeam(Team team) {
		this.team = team;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getId());
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof Player) {
			super.equals(other);
		}
		return false;
	}
}
