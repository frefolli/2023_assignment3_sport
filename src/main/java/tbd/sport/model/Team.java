package tbd.sport.model;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.NotBlank;

@Entity
public class Team {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@NotBlank
	private String name;
	@ManyToMany(mappedBy = "teams")
	private Set<Sponsor> sponsors;

	@OneToMany(mappedBy = "team")
	private List<Player> players;

	@OneToOne
	@JoinColumn(name = "coach_id")
	private Coach coach;

	@ManyToOne
	@JoinColumn(name = "parent_id")
	private Team parent;

	@OneToMany(mappedBy = "parent")
	private List<Team> reserves;

	@ManyToOne
	@JoinColumn(name = "stadium_id")
	private Stadium stadium;

	private String city;
	private Color color;

	protected Team() {
	}

	@Override
	public String toString() {
		return name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	static public Team empty(){
		return new Team();
	}

	public List<Player> getPlayers() {
		return List.copyOf(players);
	}

	public Coach getCoach() {
		return coach;
	}

	public void setCoach(Coach coach) {
		this.coach = coach;
	}

	public Team getParent() {
		return parent;
	}

	public void setParent(Team parent) {
		this.parent = parent;
	}

	public List<Team> getReserves() {
		return List.copyOf(reserves);
	}

	public Stadium getStadium() {
		return stadium;
	}

	public void setStadium(Stadium stadium) {
		this.stadium = stadium;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Color getColor() {
		return color;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCity() {
		return city;
	}

	public Set<Sponsor> getSponsors() {
		return sponsors;
	}

	public void setSponsors(Set<Sponsor> sponsors) {
		this.sponsors = sponsors;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	public void setReserves(List<Team> reserves) {
		this.reserves = reserves;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof Team) {
			Team ptr = (Team) other;
			return ptr.id.equals(id);
		}
		return false;
	}
}
